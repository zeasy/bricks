//
//  BKCore.h
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RACSignal+BKCore.h"
#import "BKNetwork.h"
#import "BKStorage.h"
#import "BKFileManager.h"
#import "BKLogger.h"

@interface BKCore : NSObject

@property (nonatomic, strong) BKNetwork *network;
@property (nonatomic, strong) BKStorage *storage;
@property (nonatomic, strong) BKFileManager *fileManager;
@property (nonatomic, strong) BKLogger *logger;

@end

@interface NSObject (BKCore)
@property (nonatomic, readonly) BKCore *bk_core;
@end
