//
//  BKCore.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "BKCore.h"
#import <objc/runtime.h>


@implementation BKCore

-(BKNetwork *)network {
    if (!_network) {
        _network = [[BKNetwork alloc] init];
    }
    return _network;
}

-(BKStorage *)storage {
    if (!_storage) {
        _storage = [[BKStorage alloc] init];
    }
    return _storage;
}

-(BKFileManager *)fileManager {
    if (!_fileManager) {
        _fileManager = [[BKFileManager alloc] init];
    }
    return _fileManager;
}

-(BKLogger *)logger {
    if (!_logger) {
        _logger = [[BKLogger alloc] init];
    }
    return _logger;
}

+(instancetype) sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

@end

void *NSObjectBKCoreKey;
@implementation NSObject (BKCore)

-(BKCore *)bk_core {
    return [BKCore sharedInstance];
}

@end