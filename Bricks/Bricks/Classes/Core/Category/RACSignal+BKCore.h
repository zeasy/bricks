//
//  RACSignal+BKCore.h
//  Bricks
//
//  Created by EASY on 15/9/10.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "RACSignal.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface RACSignal (BKCore)

-(id) bk_syncWithTimeout:(NSTimeInterval) timeout withError:(NSError **) error;

@end
