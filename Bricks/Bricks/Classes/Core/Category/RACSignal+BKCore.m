//
//  RACSignal+BKCore.m
//  Bricks
//
//  Created by EASY on 15/9/10.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "RACSignal+BKCore.h"

@implementation RACSignal (BKCore)

-(id) bk_syncWithTimeout:(NSTimeInterval) timeout withError:(NSError **) error {
    __block id result = nil;
    __block BOOL done = NO;
    
    __block NSError *localError;
    
    [[[self
        take:1]
       timeout:timeout onScheduler:[RACScheduler schedulerWithPriority:RACSchedulerPriorityBackground]]
     subscribeNext:^(id x) {
         result = x;
         done = YES;
     } error:^(NSError *e) {
         if (!done) {
             localError = e;
             done = YES;
         }
     } completed:^{
         done = YES;
     }];
    
    do {
        [NSRunLoop.mainRunLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0.1]];
    } while (!done);
    
    if (error != NULL) *error = localError;
    return result;
}

@end
