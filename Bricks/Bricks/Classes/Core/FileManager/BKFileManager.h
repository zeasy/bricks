//
//  BKFileManager.h
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface BKFileManager : NSObject

@property (nonatomic, strong) NSString *cachesDirectory;

-(RACSignal *) createDirectoryAtPath:(NSString *) path;

@end
