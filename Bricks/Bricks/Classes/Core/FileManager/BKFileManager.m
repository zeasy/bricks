//
//  BKFileManager.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "BKFileManager.h"

@implementation BKFileManager

-(NSString *)cachesDirectory {
    if (!_cachesDirectory) {
        NSArray *dirs = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        _cachesDirectory = [dirs firstObject];
    }
    return _cachesDirectory;
}

-(RACSignal *) createDirectoryAtPath:(NSString *) path {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        NSError *error = nil;
        BOOL b = NO;
        if (![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&b] || !b) {
            b = [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&error];
            
        }
        if (error) {
            [subscriber sendError:error];
        } else {
            [subscriber sendNext:@(b)];
            [subscriber sendCompleted];
        }
        return [RACDisposable disposableWithBlock:^{}];
    }];
}
@end
