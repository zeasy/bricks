//
//  BKNetwork.h
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <AFHTTPSessionManager.h>

@interface BKNetwork : NSObject

@property (nonatomic, strong) AFHTTPSessionManager *manager;

-(RACSignal *)GET:(NSString *)path parameters:(NSDictionary *)params;
-(RACSignal *)POST:(NSString *)path parameters:(NSDictionary *)params;
-(RACSignal *)PUT:(NSString *)path parameters:(NSDictionary *)params;
-(RACSignal *)DELETE:(NSString *)path parameters:(NSDictionary *)params;
-(RACSignal *)PATCH:(NSString *)path parameters:(NSDictionary *)params;


//download
//upload


@end
