//
//  BKNetwork.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "BKNetwork.h"
#import <AFNetworking-RACExtensions/AFHTTPSessionManager+RACSupport.h>

@implementation BKNetwork

-(AFHTTPSessionManager *)manager {
    if (!_manager) {
        _manager = [AFHTTPSessionManager manager];
    }
    return _manager;
}

-(RACSignal *)GET:(NSString *)path parameters:(NSDictionary *)params {
    return [self.manager rac_GET:path parameters:params];
}


-(RACSignal *)POST:(NSString *)path parameters:(NSDictionary *)params {
    return [self.manager rac_POST:path parameters:params];
}
-(RACSignal *)PUT:(NSString *)path parameters:(NSDictionary *)params {
    return [self.manager rac_PUT:path parameters:params];
}
-(RACSignal *)DELETE:(NSString *)path parameters:(NSDictionary *)params {
    return [self.manager rac_DELETE:path parameters:params];
}
-(RACSignal *)PATCH:(NSString *)path parameters:(NSDictionary *)params {
    return [self.manager rac_PATCH:path parameters:params];
}

@end
