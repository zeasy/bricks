//
//  BKStorage.h
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BKDatabaseStorage.h"

@interface BKStorage : NSObject

@property (nonatomic, strong) BKDatabaseStorage *database;
@property (nonatomic, strong) NSUserDefaults *defaults;

@end
