//
//  BKStorage.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "BKStorage.h"

NSString *BKStorageDefaultsKey = @"BKStorage";

@implementation BKStorage
-(BKDatabaseStorage *)database {
    if (!_database) {
        _database = [[BKDatabaseStorage alloc] init];
    }
    return _database;
}

-(NSUserDefaults *)defaults {
    if (!_defaults) {
        _defaults = [[NSUserDefaults alloc] initWithSuiteName:BKStorageDefaultsKey];
    }
    return _defaults;
}

@end
