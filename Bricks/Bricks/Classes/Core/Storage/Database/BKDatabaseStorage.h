//
//  BKDatabaseStorage.h
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "BKDatabaseStorageModel.h"
#import <BZObjectStore/BZObjectStore.h>

extern NSString *BKDatabaseStorageNameKey;

@interface BKDatabaseStorage : NSObject

@property (nonatomic, strong) BZObjectStore *objectStore;

-(RACSignal *) fetchWithCondition:(BZObjectStoreConditionModel *) condition forClass:(Class<BKDatabaseStorageModel>) clazz;
-(RACSignal *) fetchAllForClass:(Class<BKDatabaseStorageModel>) clazz;
-(RACSignal *) fetchById:(NSString *) bkid forClass:(Class<BKDatabaseStorageModel>) clazz;

-(RACSignal *) deleteWithCondition:(BZObjectStoreConditionModel *) condition forClass:(Class<BKDatabaseStorageModel>) clazz;
-(RACSignal *) deleteAllForClass:(Class<BKDatabaseStorageModel>) clazz;
-(RACSignal *) deleteById:(NSString *) bkid forClass:(Class<BKDatabaseStorageModel>) clazz;

-(RACSignal *) save:(id<BKDatabaseStorageModel>) model;
-(RACSignal *) refresh:(id<BKDatabaseStorageModel>) model;

@end
