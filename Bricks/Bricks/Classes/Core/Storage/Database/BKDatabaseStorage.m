//
//  BKDatabaseStorage.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "BKDatabaseStorage.h"
#import <BZObjectStore/BZActiveRecord.h>
#import <RACEXTKeyPathCoding.h>
#import "BKCore.h"

NSString *BKDatabaseStorageNameKey = @"bk.sqlite";
NSString *BKDatabaseStoragePathKey = @"Database";

@implementation BKDatabaseStorage

- (instancetype)init
{
    self = [super init];
    if (self) {
        [BZActiveRecord setupWithObjectStore:self.objectStore];
    }
    return self;
}

-(BZObjectStore *)objectStore {
    if (!_objectStore) {
        NSString *path = [self.bk_core.fileManager.cachesDirectory stringByAppendingPathComponent:BKDatabaseStoragePathKey];
        NSError *error = nil;
        [[self.bk_core.fileManager createDirectoryAtPath:path] bk_syncWithTimeout:10 withError:&error];
        path = [path stringByAppendingPathComponent:BKDatabaseStorageNameKey];
        _objectStore = [BZObjectStore openWithPath:path error:&error];
        if (error) {
            NSLog(@"%@",error);
        } 
    }
    return _objectStore;
}

-(RACSignal *) fetchWithCondition:(BZObjectStoreConditionModel *) condition forClass:(Class<BKDatabaseStorageModel>) clazz {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        Class clz = clazz;
        [clz fetchInBackground:condition completionBlock:^(NSArray *objects, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:objects];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

-(RACSignal *) fetchAllForClass:(Class<BKDatabaseStorageModel>) clazz {
    BZObjectStoreConditionModel *condition = [[BZObjectStoreConditionModel alloc] init];
    return [self fetchWithCondition:condition forClass:clazz];
}

-(RACSignal *) fetchById:(NSString *) bkid forClass:(Class<BKDatabaseStorageModel>) clazz {
    BKDatabaseStorageModel *model = nil;
    NSString *bkIdKey = @keypath(model,bkId);
    BZObjectStoreConditionModel *condition = [[BZObjectStoreConditionModel alloc] init];
    condition.sqlite.where = [NSString stringWithFormat:@"%@ = '%@'",bkIdKey,bkid];
    condition.sqlite.limit = @1;
    return [[self fetchWithCondition:condition forClass:clazz] map:^id(NSArray *models) {
        return [models firstObject];
    }];
}

-(RACSignal *) deleteWithCondition:(BZObjectStoreConditionModel *) condition forClass:(Class<BKDatabaseStorageModel>) clazz {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        Class clz = clazz;
        [clz deleteAllInBackground:condition completionBlock:^(NSError *error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:@YES];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

-(RACSignal *) deleteAllForClass:(Class<BKDatabaseStorageModel>) clazz {
    BZObjectStoreConditionModel *condition = [[BZObjectStoreConditionModel alloc] init];
    return [self deleteWithCondition:condition forClass:clazz];
}

-(RACSignal *) deleteById:(NSString *) bkid forClass:(Class<BKDatabaseStorageModel>) clazz {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        BKDatabaseStorageModel *model = nil;
        Class clz = clazz;
        NSString *bkIdKey = @keypath(model,bkId);
        BZObjectStoreConditionModel *condition = [[BZObjectStoreConditionModel alloc] init];
        condition.sqlite.where = [NSString stringWithFormat:@"%@ = '%@'",bkIdKey,bkid];
        condition.sqlite.limit = @1;
        [clz deleteAllInBackground:condition completionBlock:^(NSError *error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:@YES];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

-(RACSignal *) save:(id<BKDatabaseStorageModel>) model {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        BKDatabaseStorageModel *m = (BKDatabaseStorageModel *)model;
        [m saveInBackground:^(NSError *error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:@YES];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

-(RACSignal *) refresh:(id<BKDatabaseStorageModel>) model {
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        BKDatabaseStorageModel *m = (BKDatabaseStorageModel *)model;
        [m refreshInBackground:^(NSObject *object, NSError *error) {
            if (error) {
                [subscriber sendError:error];
            } else {
                [subscriber sendNext:object];
                [subscriber sendCompleted];
            }
        }];
        return [RACDisposable disposableWithBlock:^{
        }];
    }];
}

@end
