//
//  BKDatabaseStorageModel.h
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BKDatabaseStorageModel <NSObject>
@required
@property (nonatomic, strong) NSString *bkId;
@end

@interface BKDatabaseStorageModel : NSObject <BKDatabaseStorageModel>

@property (nonatomic, strong) NSString *bkId;

@end
