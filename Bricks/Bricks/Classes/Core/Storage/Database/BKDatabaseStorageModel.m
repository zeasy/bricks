//
//  BKDatabaseStorageModel.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "BKDatabaseStorageModel.h"

@implementation BKDatabaseStorageModel

-(NSString *)bkId {
    if (!_bkId) {
        _bkId = [NSUUID UUID].UUIDString;
    }
    return _bkId;
}

@end
