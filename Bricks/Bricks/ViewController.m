//
//  ViewController.m
//  Bricks
//
//  Created by EASY on 15/9/9.
//  Copyright (c) 2015年 zeasy. All rights reserved.
//

#import "ViewController.h"
#import "BKCore.h"
#import "BKDatabaseStorageModel.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSError *error = nil;
    id o = [[self.bk_core.network GET:@"http://www.baidu.com" parameters:@{}] bk_syncWithTimeout:10 withError:&error];
    NSLog(@"%@ %@",o,error);
    
    BKDatabaseStorageModel *model = [[BKDatabaseStorageModel alloc] init];
    [[self.bk_core.storage.database fetchAllForClass:[BKDatabaseStorageModel class]] subscribeNext:^(id x) {
        NSLog(@"%@",x);
    } error:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
    [self.bk_core.storage.defaults setObject:@"1" forKey:@"A"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
